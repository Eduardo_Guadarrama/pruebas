﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Encuentralo.Controllers.Base;
using Encuentralo.Models;
using Microsoft.AspNet.Identity.Owin;

namespace Encuentralo.Controllers
{
    public class InfoController : BaseController
    {
        // GET: Info
        [AllowAnonymous]
        public ActionResult Index()
        {
            //var Db = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            var listUsers = Db.Users.ToList();
            var listRoles = Db.Roles.ToList();
            //List fron Users
            ViewBag.ListUsers = listUsers;
            //List from Roles
            ViewBag.ListRoles = listRoles;

            ViewBag.Saludo = "Hola desde el controlador";
            return View();
        }
    }
}