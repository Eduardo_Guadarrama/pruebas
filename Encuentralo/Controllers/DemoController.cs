﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;

namespace Encuentralo.Controllers
{
    public class DemoController : BaseController
    {
        public DemoController() : base()
        {

        }
        // GET: Demo
        public ActionResult Index()
        {
            var list = Db.Users;
            var roles = Db.Roles;

            return View();
        }
    }
}